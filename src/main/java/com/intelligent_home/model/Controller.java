package com.intelligent_home.model;

import javax.persistence.*;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

@Entity
@Table
public class Controller {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public Controller() {
    }

    public Controller(long id) {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
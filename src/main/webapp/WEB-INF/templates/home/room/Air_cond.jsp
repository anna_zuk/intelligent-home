<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Air Conditioning</title>
</head>
<body ng-app="homeapp">
    <%@include file="/WEB-INF/includes/LeftMenu.jsp"%>
    <%@include file="/WEB-INF/includes/RightMenu.jsp"%>
<nav>
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Adjust air conditioning in {{room.roomType}}</li>
    </ul>
</nav>
<div ng-controller="HomeController as ctrl">
    <div class="container">
        <div class="de">
            <div class="den">
                <hr class="line3">
                <div class="switch3">
                    <label for="switch_on"><span>On</span></label>
                    <label for="switch_off"><span>Off</span></label>
                    <input id="switch_on" name="switch" ng-click="setAir(1,true)" checked type="radio">
                    <input id="switch_off" name="switch" ng-click="setAir(1,false)" type="radio">
                    <div class="light"><span></span></div>
                    <div class="dot"><span></span></div>
                    <div class="dene">
                        <div class="denem">
                            <div class="deneme">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
</body>
</html>
package com.intelligent_home.model.responses;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

public class Response {
    private String responseUUID;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime time;

    private Result result;

    public Response() {
        this.responseUUID = UUID.randomUUID().toString();
        this.time = LocalDateTime.now();
        this.result = new Result();
    }

    public Response(Result result) {
        this.responseUUID = UUID.randomUUID().toString();
        this.time = LocalDateTime.now();
        this.result = result;
    }

    public String getResponseUUID() {
        return responseUUID;
    }

    public void setResponseUUID(String responseUUID) {
        this.responseUUID = responseUUID;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Response{" +
                "responseUUID='" + responseUUID + '\'' +
                ", time=" + time +
                ", result='" + result + '\'' +
                '}';
    }
}

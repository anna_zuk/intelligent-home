package com.intelligent_home.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

@Controller
@RequestMapping(value = "/")
public class IndexController {

    @RequestMapping(value = "/")
    public String index(){
        return "Index";
    }

    @RequestMapping(value = "/login_form")
    public String login_form(){
        return "templates/registration/Login_form";
    }

    @RequestMapping(value = "/login_info")
    public String login_info(){
        return "templates/registration/Login_info";
    }

    @RequestMapping(value = "/logout")
    public String logout(){
        return "templates/registration/Logout";
    }

    @RequestMapping(value = "/registration")
    public String registration_form(){
        return "templates/registration/Registration_form";
    }

    @RequestMapping(value = "/success")
    public String success(){
        return "templates/registration/Success";
    }

    @RequestMapping(value = "/welcome")
    public String welcome(){
        return "templates/registration/Welcome";
    }

    @RequestMapping(value = "/terms")
    public String terms() {
        return "templates/terms/Terms"; }

    @RequestMapping(value = "/terms_2")
    public String terms_2() {
        return "templates/terms/Terms_2"; }

    @RequestMapping(value = "/terms_3")
    public String terms_3() {
        return "templates/terms/Terms_3"; }

    @RequestMapping(value = "/terms_4")
    public String terms_4() {
        return "templates/terms/Terms_4"; }

    @RequestMapping(value = "/terms_5")
    public String terms_5() {
        return "templates/terms/Terms_5"; }

    @RequestMapping(value = "/terms_6")
    public String terms_6() {
        return "templates/terms/Terms_6"; }

    @RequestMapping(value = "/tv")
    public String tv(){
        return "templates/home/TV";
    }

    @RequestMapping(value="/alarm_clock")
    public String alarmClock(){
        return "templates/home/Alarm_clock";
    }

    @RequestMapping(value="/internet_control")
    public String internetControl(){
        return "templates/home/Internet_control";
    }

    @RequestMapping(value="/edit_home")
    public String editHome(){
        return "templates/home/Edit_home";
    }

    @RequestMapping(value="/remove_home")
    public String removeHome(){
        return "templates/home/Remove_home";
    }

    @RequestMapping(value="/locks")
    public String locks(){
        return "templates/home/Locks";
    }

    @RequestMapping(value="/my_homes")
    public String myHome(){
        return "templates/home/My_home";
    }

    @RequestMapping(value="/add_home")
    public String addHome(){
        return "templates/home/Add_home";
    }

    @RequestMapping(value="/air_cond")
    public String airCond(){
        return "templates/home/room/Air_cond";
    }

    @RequestMapping(value="/light")
    public String lights(){
        return "templates/home/room/Light";
    }

    @RequestMapping(value="/temperature")
    public String temperature(){
        return "templates/home/room/Temperature";
    }

    @RequestMapping(value="/user_profile")
    public String userProfile(){
        return "templates/user/User_profile";
    }

    @RequestMapping(value="/settings")
    public String Settings(){
        return "templates/user/Settings";
    }
}


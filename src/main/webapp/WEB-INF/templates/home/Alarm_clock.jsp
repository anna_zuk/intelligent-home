<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Alarm Clock</title>
</head>
<body>
    <%@include file="/WEB-INF/includes/LeftMenu.jsp"%>
<nav class="inner">
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Remote Alarm Clock Control Panel</li>
    </ul>
</nav>
<div class="container2">
    <div class="clock-wrap">
        <div class="displays mode-display">24hr clock</div>
        <div class="displays ampm-display handle">am</div>
        <div class="clock-display">
            <ul class="control-hrs handle">
                <li>+</li>
                <li>-</li>
            </ul>
            <div class="hrs nums">00</div><span class="nums">:</span>
            <div class="mins nums">00</div>
            <ul class="control-mins handle">
                <li>+</li>
                <li>-</li>
            </ul>
        </div>
        <div class="displays alarm-display">no alarm set</div>
    </div>
    <div class="btn-wrap">
        <div class="alarm">set alarm</div>
        <div class="mode">mode</div>
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
    <script src="/static/js/style/alarm.js"></script>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Terms</title>
</head>
<body>
    <%@include file="/WEB-INF/includes/Logo_Background.jsp"%>
<div class="box_terms">
    <%@include file="/WEB-INF/includes/TermsMenu.jsp"%>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>5. Limitations</h3>
    <br><br>
    <p class="Terms"> In no event shall KOTI&reg;, nor any of its officers, directors and employees, be liable to You for anything arising out of or in any way connected with Your use of this Website, whether such liability is under contract, tort or otherwise, and KOTI&reg;, including its officers, directors and employees shall not be liable for any indirect, consequential or special liability arising out of or in any way related to Your use of this Website.</p>
    <br><br><br>
</div>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>My Homes</title>
</head>
<body ng-app="homeapp">
    <%@include file="/WEB-INF/includes/LeftMenu.jsp"%>
    <%@include file="/WEB-INF/includes/RightMenu2.jsp"%>
<div ng-controller="HomeController as ctrl">
    <div class="container5">
        <div ng-repeat="home in ctrl.homes">
            <nav>
                <ul>
                    <li><span ng-bind="home.name"></span></li>
                </ul>
                <div class="icons_a">
                    <a href="tv"><i class="material-icons">&#xe333;</i><span>TV</span></a>
                    <a href="locks"><i class="material-icons">&#xe897;</i><span>Locks</span></a>
                    <a href="alarm_clock"><i class="material-icons">&#xe190;</i><span>Alarm Clock</span></a>
                    <a href="internet_control"><i class="material-icons">&#xe328;</i><span>Internet Control</span></a>
                </div>
                <div class="icons">
                    <div ng-repeat="room in home.rooms">
                        <a href="temperature"><i class="material-icons">{{room.roomType.icon}}</i><span ng-bind="room.name"></span></a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
</body>
</html>
package com.intelligent_home.service;

import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;

import java.util.List;
import java.util.Optional;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

public interface UserService {
    // user story - chce sprawdzić czy dany użytkownik (o danym loginie) już istnieje, więc
    // potrzebuje metody do sprawdzania tego:
    boolean userExists(String login);

    // user story - chcę zarejestrować użytkownika
    boolean registerUser(User userToRegister, UserData data);

    // user story - chcę wylistować wszystkich użytkowników
    List<User> getAllUsers();

    // user story - logowanie użytkownika
    boolean userLogin(String login, String passwordHash);

    Optional<User> getUserWithId(Long id);

    void persistUser(User userTransferring);
}
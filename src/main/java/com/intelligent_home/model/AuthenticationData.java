package com.intelligent_home.model;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-02-02
 */

public class AuthenticationData {
    private String email;
    private String password;
    private boolean remember;

    public AuthenticationData() {
    }

    public AuthenticationData(String email, String password, boolean remember) {
        this.email = email;
        this.password = password;
        this.remember = remember;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

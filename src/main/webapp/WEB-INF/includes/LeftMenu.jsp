<div id="cont">
    <div id="menu-fixed">
        <a href="#cont">
            <i class="material-icons back">&#xE314;</i>
        </a>
        <p><a href="/">KOTI</a></p>
        <a href="#menu-fixed">
            <div class="logo">
                <span></span>
            </div>
            <p class="pmenu">MENU</p>
        </a>
        <hr>
        <ul class="menu">
            <a href="my_homes"><li><i class="material-icons">&#xE88A;</i><p>My Homes</p></li></a>
            <a href="logout"><li><i class="material-icons">&#xe879;</i><p>Logout</p></li></a>
        </ul>
    </div>
</div>
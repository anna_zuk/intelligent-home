<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Terms</title>
</head>
<body>
    <%@include file="/WEB-INF/includes/Logo_Background.jsp"%>
<div class="box_terms">
    <%@include file="/WEB-INF/includes/TermsMenu.jsp"%>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>2. Property Rights</h3>
    <br><br>
    <p class="Terms"> Other than content You own, which You may have opted to include on this Website, under these Terms, KOTI&reg; and/or its licensors own all rights to the intellectual property and material contained in this Website, and all such rights are reserved.</p><br><br>
    <p class="Terms"> You are granted a limited license only, subject to the restrictions provided in these Terms, for purposes of viewing the material contained on this Website.</p>
    <br><br><br>
</div>
</body>
</html>
'use strict';

angular.module('homeapp').controller('SessionController', ['$scope', '$window', '$cookies',
    function ($scope, $window, $cookies) {
        var self = this;

        console.log('Session controller created.');

        $scope.isLoggedIn = function() {
            var sessionCookieContent = $cookies.get('session_cookie');

            if(sessionCookieContent===undefined){
                $window.location.href = "/login"
            }
        }

        $scope.login = function(sessionId) {
            console.log('Logging in with sessionId = ' + sessionId);
            var a = new Date();
            a = new Date(a.getTime() + 360000);
            console.log('Expiration date:' + a.toGMTString());
            $cookies.put('session_cookie', sessionId, {expires: '' + a.toGMTString()});
            $window.location.href= "/getUserList";
        }
    }]);
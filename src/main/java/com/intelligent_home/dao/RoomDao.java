package com.intelligent_home.dao;

import com.intelligent_home.model.*;
import com.sun.istack.internal.NotNull;

import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-01-10
 */

public interface RoomDao {

    List<Long> getAllRoomsByUser(long userId);

    Room getRoom(long roomId);

    List<Sensor> getRoomSensors(long roomId);

    List<Controller> getRoomControllers(long roomId);

    Sensor getSensor(long sensorId);

    Controller getController(long controllerId);

    boolean roomExists(@NotNull String roomName);

    void addRoom(long userId, long homeId, Room room);

    void addController(long userId, long roomId, Controller controller);

    void addSensor(long userId, long roomId, Sensor sensor);
}
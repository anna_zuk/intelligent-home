<div id="cont2">
    <div id="menu-fixed2">
        <a href="#cont2">
            <i class="material-icons back">&#xe315;</i>
        </a>
        <a href="#menu-fixed2">
            <div class="logo2">
                <span></span>
                <p></p>
            </div>
            <p class="pmenu2">NAVIGATION</p>
        </a>
        <hr>
        <ul class="menu2">
            <a href="temperature"><li><i class="material-icons">&#xe430;</i><p>Temperature</p></li></a>
            <a href="light"><li><i class="material-icons">&#xe90f;</i><p>Lights</p></li></a>
            <a href="air_cond"><li><i class="material-icons">&#xe915;</i><p>Air Conditioning</p></li></a>
        </ul>
    </div>
</div>
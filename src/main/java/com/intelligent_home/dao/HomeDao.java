package com.intelligent_home.dao;

import com.intelligent_home.model.Home;
import com.intelligent_home.model.Room;
import com.sun.istack.internal.NotNull;

import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-01-10
 */

public interface HomeDao {
    List<Home> getAllHomes();

    Home getHomeById(long homeId);

    boolean homeExists(@NotNull String homeName);

    void addHome(long userId, Home home);

    Room getRoomById(long id);

    void updateRoom(Room room);
}
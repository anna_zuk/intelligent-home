'use strict';

angular.module('homeapp').controller('HomeController', ['$scope','HomesService',
    function ($scope, HomesService) {
        var self = this;
        self.homes = [];
        self.rooms = [];

        console.log('Controller for homes created.');
        self.fetchHomes = fetchHomes;
        console.log('Controller for rooms created.');
        self.fetchRooms = fetchRooms;

        fetchHomes();
        fetchRooms();

        $scope.updateTemperature = function(id, temp) {
            console.log('Executed.');
            console.log('Fetching homes.');
            HomesService.updateTemperature(id, temp).then(
                function (response) {
                    console.log('Temperature updated.');
                },
                function (result) {
                    console.log('Error.');
                }
            );
        }

        $scope.updateLight = function(id, light) {
            console.log('Executed.');
            console.log('Fetching homes.');
            HomesService.updateLight(id, light).then(
                function (response) {
                    console.log('Light updated.');
                },
                function (result) {
                    console.log('Error.');
                }
            );
        }

        $scope.setAir = function(id, air) {
            console.log('Executed.');
            console.log('Fetching homes.');
            HomesService.setAir(id, air).then(
                function (response) {
                    console.log('Air conditioning set.');
                },
                function (result) {
                    console.log('Error.');
                }
            );
        }

        $scope.setLocks = function(id, locks) {
            console.log('Executed.');
            console.log('Fetching homes.');
            HomesService.setLocks(id, locks).then(
                function (response) {
                    console.log('Locks set.');
                },
                function (result) {
                    console.log('Error.');
                }
            );
        }

        $scope.setInternet = function(id, internet) {
            console.log('Executed.');
            console.log('Fetching homes.');
            HomesService.setInternet(id, internet).then(
                function (response) {
                    console.log('Internet set.');
                },
                function (result) {
                    console.log('Error.');
                }
            );
        }

        function fetchHomes() {
            console.log('Fetching homes.');
            HomesService.fetchAllHomes().then(
                function (response) {
                    console.log(response);
                    self.homes = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchRoom(roomId, homeId) {
            console.log('Fetching room: ' + roomId);
            HomesService.fetchRoom(roomId, homeId).then(
                function (response) {
                    console.log(response);
                    self.homes = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchRooms() {
            console.log('Fetching rooms.');
            HomesService.fetchAllRooms().then(
                function (response) {
                    console.log(response);
                    self.homes = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

    }]);
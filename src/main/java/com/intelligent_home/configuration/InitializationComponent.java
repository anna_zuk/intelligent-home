package com.intelligent_home.configuration;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-02-02
 */

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class InitializationComponent {
    @Bean
    InitializingBean sendDatabase() {
        return () -> {
            Logger.getLogger(getClass()).info("Completed creating bean.");
        };
    }
}

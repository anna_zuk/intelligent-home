<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Temperature</title>
</head>
<body ng-app="homeapp">
    <%@include file="/WEB-INF/includes/LeftMenu.jsp"%>
    <%@include file="/WEB-INF/includes/RightMenu.jsp"%>
<nav>
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Adjust temperature in {{room.roomType}}</li>
    </ul>
</nav>
<div ng-controller="HomeController as ctrl">
    <div class="container">
        <div class="de">
            <div class="den">
                <hr class="line">
                <hr class="line">
                <hr class="line">
                <div class="switch">
                    <label for="switch_0"><span>18</span></label>
                    <label for="switch_1"><span>20</span></label>
                    <label for="switch_2"><span>22</span></label>
                    <label for="switch_3"><span>24</span></label>
                    <label for="switch_4"><span>26</span></label>
                    <label for="switch_5"><span>28</span></label>
                    <input id="switch_0" name="switch" ng-click="updateTemperature(1,18)" checked type="radio">
                    <input id="switch_1" name="switch" ng-click="updateTemperature(1,20)" type="radio">
                    <input id="switch_2" name="switch" ng-click="updateTemperature(1,22)" type="radio">
                    <input id="switch_3" name="switch" ng-click="updateTemperature(1,24)" type="radio">
                    <input id="switch_4" name="switch" ng-click="updateTemperature(1,26)" type="radio">
                    <input id="switch_5" name="switch" ng-click="updateTemperature(1,28)" type="radio">
                    <div class="light"><span></span></div>
                    <div class="dot"><span></span></div>
                    <div class="dene">
                        <div class="denem">
                            <div class="deneme">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
</body>
</html>
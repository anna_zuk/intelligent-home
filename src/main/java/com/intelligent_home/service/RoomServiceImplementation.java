package com.intelligent_home.service;

import com.intelligent_home.dao.RoomDao;
import com.intelligent_home.model.Controller;
import com.intelligent_home.model.Room;
import com.intelligent_home.model.Sensor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-01-10
 */

@Service(value = "roomService")
@Transactional
public class RoomServiceImplementation implements RoomService {

    private static final Logger LOG = Logger.getLogger(RoomServiceImplementation.class);

    @Autowired
    RoomDao roomDao;

    @Override
    public List<Long> getAllRoomsByUser(long userId) {
        return roomDao.getAllRoomsByUser(userId);
    }

    @Override
    public Room getRoom(long roomId) {
        return roomDao.getRoom(roomId);
    }

    @Override
    public List<Sensor> getRoomSensors(long roomId) {
        return roomDao.getRoomSensors(roomId);
    }

    @Override
    public List<Controller> getRoomControllers(long roomId) {
        return roomDao.getRoomControllers(roomId);
    }

    @Override
    public Sensor getSensor(long sensorId) {
        return roomDao.getSensor(sensorId);
    }

    @Override
    public Controller getController(long controllerId) {
        return roomDao.getController(controllerId);
    }

    @Override
    public boolean roomExists(String roomName) {
        return roomDao.roomExists(roomName);
    }

    @Override
    public boolean addRoom(long userId, long homeId, Room room) {
        roomDao.addRoom(userId, homeId, room);
        return true;
    }

    @Override
    public boolean addController(long userId, long roomId, Controller controller) {
        roomDao.addController(userId, roomId, controller);
        return true;
    }

    @Override
    public boolean addSensor(long userId, long roomId, Sensor sensor) {
        roomDao.addSensor(userId, roomId, sensor);
        return true;
    }
}

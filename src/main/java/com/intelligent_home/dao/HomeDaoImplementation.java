package com.intelligent_home.dao;

import com.intelligent_home.model.Home;
import com.intelligent_home.model.Room;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-01-10
 */

@Repository(value = "homeDao")
public class HomeDaoImplementation extends AbstractDao implements HomeDao {

    @Override
    public List<Home> getAllHomes() {
        Criteria sqlRequest = getSession().createCriteria(Home.class);
        return sqlRequest.list();
    }

    @Override
    public Home getHomeById(long homeId) {
        Criteria sqlRequest = getSession().createCriteria(Home.class).add(Restrictions.eq("id", homeId));

        return (Home) sqlRequest.uniqueResult();
    }

    @Override
    public boolean homeExists(String homeName) {
        long resultCount = (long) getSession().createCriteria(Home.class)
                .add(Restrictions.eq("name", homeName).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount > 0;
    }

    @Override
    public void addHome(long userId, Home home) {
        persist(home);
    }

    @Override
    public Room getRoomById(long id) {
        Criteria sqlRequest = getSession().createCriteria(Room.class).add(Restrictions.eq("id", id));
        return (Room) sqlRequest.uniqueResult();
    }

    @Override
    public void updateRoom(Room room) {
        getSession().update(room);
    }
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>TV</title>
</head>
<body>
    <%@include file="/WEB-INF/includes/LeftMenu.jsp"%>
<nav class="inner">
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Remote TV Control Panel</li>
    </ul>
</nav>
<div class="container3" data-behaviour="search-on-list">
    <input type="text2" class="input-query" data-search-on-list="search" placeholder="Search channel..."/>
    <span class="counter" data-search-on-list="counter"></span>
    <div class="list-wrap">
        <ul class="list" data-search-on-list="list">
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP1</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP2</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Polsat</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Polsat FILM</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Polsat 2</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVN</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVN 7</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVN 24</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TV 4</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TV Puls</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Historia</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Rozrywka</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Polonia</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Sport</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Tele 5</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>HBO</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>HBO 2</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Comedy Central</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Animal Planet</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>MTV Polska</span>
            </li>
        </ul>
    </div>
    <div class="toggle">
        <input type="checkbox">
        <span class="button"></span>
        <span class="label"><i class="material-icons">&#xe8ac;</i></span>
    </div>
    <div class="container7">
        <span class="label"><i class="material-icons">&#xe050;</i></span>
        <input type="range" value="0.6" data-steps="8" id="sliderInput" />
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
    <script src="/static/js/style/search.js"></script>
    <script src="/static/js/style/power_btn.js"></script>
    <script src="/static/js/style/volume.js"></script>
</body>
</html>
package com.intelligent_home.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

@Entity
@Table(name = "app_user")
public class User {
    @Id
    @Column(name="app_user_id")
    @GeneratedValue
    private long id;

    @Column
    private String login;

    @Column
    private String passwordHash;

    @OneToMany(fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Home> homeList;

    @OneToOne(cascade = CascadeType.PERSIST)
    private UserData userData;

    public User() {
        this.id = 0L;
        this.homeList = new ArrayList<>();
    }

    public User(String login, String passwordHash) {
        this.id = 0L;
        this.login = login;
        this.passwordHash = passwordHash;
        this.homeList = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public List<Home> getHomeList() {
        return homeList;
    }

    public void setHomeList(List<Home> homeList) {
        this.homeList = homeList;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                '}';
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Login Form</title>
</head>
<body ng-app="homeapp">
    <%@include file="/WEB-INF/includes/Logo_Background.jsp"%>
<div ng-controller="UserController as ctrl">
    <div class="box_login">
        <h1>Login</h1>
        <form method="post" action="login">
            <hr>
            <input type="text" id="userEmail" placeholder="Email" required autofocus/>
            <input type="password" id="userPassword" placeholder="Password" required/>
            <button type="submit" class="login">Login</button>
            <br><br><br><br>
            <hr>
            <h4>Not registered yet?</h4>
            <button type="button" onclick="window.location.href = '/';" class="login">Register</button>
        </form>
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
</body>
</html>
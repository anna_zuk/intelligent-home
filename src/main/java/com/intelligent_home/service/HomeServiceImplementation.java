package com.intelligent_home.service;

import com.intelligent_home.dao.HomeDao;
import com.intelligent_home.model.Home;
import com.intelligent_home.model.Room;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-01-10
 */

@Service(value = "homeService")
@Transactional
public class HomeServiceImplementation implements HomeService {

    private static final Logger LOG = Logger.getLogger(HomeServiceImplementation.class);

    @Autowired
    HomeDao homeDao;

    @Override
    public List<Home> getAllHomes() {
        return homeDao.getAllHomes();
    }

    @Override
    public List<Long> getHomeRooms(long homeId) {
        Home home = getHomeById(homeId);

        List<Long> roomsIds = new ArrayList<>();
        for (Room r: home.getRooms()) {
            roomsIds.add(r.getId());
        }
        return roomsIds;
    }

    @Override
    public Home getHomeById(long homeId) {
        return homeDao.getHomeById(homeId);
    }

    @Override
    public boolean homeExists(String homeName) {
        return homeDao.homeExists(homeName);
    }

    @Override
    public boolean addHome(long userId, Home home) {
        homeDao.addHome(userId, home);
        return true;
    }

    @Override
    public void updateRoomTemperature(long roomId, int temperature) {
        Room room = homeDao.getRoomById(roomId);
        room.setTemperature(temperature);

        homeDao.updateRoom(room);
    }

    @Override
    public void updateRoomLight(long roomId, String light) {
        Room room = homeDao.getRoomById(roomId);
        room.setLight(light);

        homeDao.updateRoom(room);
    }

    @Override
    public void setAir(long roomId, boolean air) {
        Room room = homeDao.getRoomById(roomId);
        room.setAir(air);

        homeDao.updateRoom(room);
    }

    @Override
    public void setLocks(long roomId, boolean locks) {
        Room room = homeDao.getRoomById(roomId);
        room.setLocks(locks);

        homeDao.updateRoom(room);
    }

    @Override
    public void setInternet(long roomId, boolean internet) {
        Room room = homeDao.getRoomById(roomId);
        room.setInternet(internet);

        homeDao.updateRoom(room);
    }
}


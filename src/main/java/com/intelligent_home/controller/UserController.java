package com.intelligent_home.controller;

import com.intelligent_home.service.UserService;
import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;
import com.intelligent_home.model.responses.Response;
import com.intelligent_home.model.responses.ResponseFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

@RestController
@RequestMapping(value = "/rest/")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/registerUser", method = RequestMethod.GET)
    public ResponseEntity<Response> registerUser(@RequestParam String userName,
                                                 @RequestParam String password) {
        Logger.getLogger(getClass()).debug("Requested registerUser with params: " +
                " " + userName + ":" + password);

        if (userService.userExists(userName)) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User exists."),
                    HttpStatus.BAD_REQUEST);
        } else {
            userService.registerUser(new User(userName, password), new UserData("email@gmail.com",true));
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/registerUserWithData", method = RequestMethod.POST)
    public ResponseEntity<Response> registerUserWithData(@RequestBody User user) {
        Logger.getLogger(getClass()).debug("Requested registerUser with user: " + user);

        if (userService.userExists(user.getLogin())) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User exists."),
                    HttpStatus.BAD_REQUEST);
        } else {
            userService.registerUser(user, user.getUserData());
            return new ResponseEntity<Response>(
                    ResponseFactory.success(user.getId()),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> requestUserList() {
        return new ResponseEntity<List<User>>(userService.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserWithId(@PathVariable Long id) {
        Optional<User> user = userService.getUserWithId(id);
        if (user.isPresent()) {
            return new ResponseEntity<Response>(ResponseFactory.success(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/userExists/{login}", method = RequestMethod.GET)
    public ResponseEntity<Response> requestUserExists(@PathVariable String login) {
        return new ResponseEntity<Response>(ResponseFactory.success(userService.userExists(login)), HttpStatus.OK);
    }
}
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Add Home</title>
</head>
<body ng-app="homeapp">
    <%@include file="/WEB-INF/includes/LeftMenu.jsp"%>
    <%@include file="/WEB-INF/includes/RightMenu2.jsp"%>
<nav>
    <ul>
        <li>Add Home</li>
    </ul>
</nav>
<div id="form">
    <form name="form-homes">
        <input class="form" type="text" id="homeName" placeholder="Home Name">
        <input class="form" type="text" id="roomName" placeholder="Room Name">
        <span class="custom-dropdown">
            <select>
                <option>Bathroom</option>
                <option>Bedroom</option>
                <option>Living Room</option>
                <option>Kitchen</option>
                <option>Child's Room</option>
                <option>Art Room</option>
                <option>Office</option>
                <option>Other</option>
            </select>
        </span>
        <br><br><br>
        <input class="form" type="submit" value="Add Home">
    </form>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
</body>
</html>
package com.intelligent_home.model;

import javax.persistence.*;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

@Entity
@Table(name = "user_data")
public class UserData {
    @Id
    @Column
    @GeneratedValue
    private long id;

    @Column(nullable = true)
    private String email;

    @Column(nullable = false)
    private boolean isFemale;

    public UserData() {
        this.id = 0L;
    }

    public UserData(String email, boolean isFemale) {
        this.id = 0L;
        this.email = email;
        this.isFemale = isFemale;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isFemale() {
        return isFemale;
    }

    public void setFemale(boolean female) {
        isFemale = female;
    }
}
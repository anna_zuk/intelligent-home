package com.intelligent_home.service;

import com.intelligent_home.dao.UserDao;
import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

@Service(value = "userService")
@Transactional
public class UserServiceImplementation implements UserService {

    @Autowired
    UserDao userDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImplementation.class);

    @Override
    public boolean userExists(String login) {
        return userDao.userExists(login);
    }

    @Override
    public boolean registerUser(User userToRegister, UserData data) {
        userDao.registerUser(userToRegister, data);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public boolean userLogin(String login, String passwordHash) {
        Optional<User> loggingInUser = userDao.findByLogin(login);
        // weryfikacja loginu
        if (loggingInUser.isPresent()) {
            User user = loggingInUser.get();

            // weryfikacja hasła
            if(user.getPasswordHash().equals(passwordHash)){
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Optional<User> getUserWithId(Long id) {
        return userDao.findById(id);
    }

    @Override
    public void persistUser(User userTransferring) {
        userDao.save(userTransferring);
    }
}
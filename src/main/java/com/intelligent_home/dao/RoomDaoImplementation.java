package com.intelligent_home.dao;

import com.intelligent_home.model.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-01-10
 */

@Repository(value = "roomDao")
public class RoomDaoImplementation extends AbstractDao implements RoomDao {

    @Override
    public List<Long> getAllRoomsByUser(long userId) {
        Criteria sqlRequest = getSession().createCriteria(User.class).add(Restrictions.eq("id", userId));
        return sqlRequest.list();
    }

    @Override
    public Room getRoom(long roomId) {
        Criteria sqlRequest = getSession().createCriteria(Room.class).add(Restrictions.eq("id", roomId));
        return (Room) sqlRequest.uniqueResult();
    }

    @Override
    public List<Sensor> getRoomSensors(long roomId) {
        Criteria sqlRequest = getSession().createCriteria(Room.class).add(Restrictions.eq("id", roomId));
        return sqlRequest.list();
    }

    @Override
    public List<Controller> getRoomControllers(long roomId) {
        Criteria sqlRequest = getSession().createCriteria(Room.class).add(Restrictions.eq("id", roomId));
        return sqlRequest.list();
    }

    @Override
    public Sensor getSensor(long sensorId) {
        Criteria sqlRequest = getSession().createCriteria(Sensor.class).add(Restrictions.eq("id", sensorId));
        if(sqlRequest!=null) {
            return (Sensor) sqlRequest.uniqueResult();
        }else{
            throw new NullPointerException("This sensor does not exist.");
        }
    }

    @Override
    public Controller getController(long controllerId) {
        Criteria sqlRequest = getSession().createCriteria(Controller.class).add(Restrictions.eq("id", controllerId));
        if(sqlRequest!=null) {
            return (Controller) sqlRequest.uniqueResult();
        }else{
            throw new NullPointerException("This controller does not exist.");
        }
    }

    @Override
    public boolean roomExists(String roomName) {
        long resultCount = (long) getSession().createCriteria(Room.class)
                .add(Restrictions.eq("name", roomName).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount > 0;
    }

    @Override
    public void addRoom(long userId, long homeId, Room room) {
        persist(room);
    }

    @Override
    public void addController(long userId, long roomId, Controller controller) {
        persist(controller);
    }

    @Override
    public void addSensor(long userId, long roomId, Sensor sensor) {
        persist(sensor);
    }
}

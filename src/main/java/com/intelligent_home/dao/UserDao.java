package com.intelligent_home.dao;

import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;
import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Optional;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 */

public interface UserDao {
    void registerUser(User u, UserData data);

    Optional<User> findById(Long id);

    boolean userExists(String withLogin);

    List<User> getAllUsers();

    void save(User userTransferring);

    Optional<User> findByLogin(String login);
}

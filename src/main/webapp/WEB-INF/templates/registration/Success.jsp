<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Success</title>
</head>
<body>
    <%@include file="/WEB-INF/includes/Logo_Background.jsp"%>
<div class="box_success">
    <br><br><br>
    <form method="post" action="login">
        <h4><b><font color="green">You have registered successfully!</font></b></h4>
        <br><br><br>
        <hr>
        <h4>Please login here:</h4>
        <a href="login_form" class="button_log">Login</a>
    </form>
</div>
</body>
</html>
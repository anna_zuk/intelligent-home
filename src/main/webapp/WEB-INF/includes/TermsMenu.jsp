<div class="skew-menu">
    <ul>
        <li><a href="terms">Introduction</a></li>
        <li><a href="terms_2">Property Rights</a></li>
        <li><a href="terms_3">Restrictions</a></li>
        <li><a href="terms_4">No Warranties</a></li>
        <li><a href="terms_5">Limitations</a></li>
        <li><a href="terms_6">Assignment</a></li>
    </ul>
</div>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Lights</title>
</head>
<body ng-app="homeapp">
    <%@include file="/WEB-INF/includes/LeftMenu.jsp"%>
    <%@include file="/WEB-INF/includes/RightMenu.jsp"%>
<nav>
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Adjust light in {{room.roomType}}</li>
    </ul>
</nav>
<div ng-controller="HomeController as ctrl">
    <div class="container">
        <div class="de">
            <div class="den">
                <hr class="line2">
                <hr class="line2">
                <div class="switch2">
                    <label for="switch_a"><span>Full</span></label>
                    <label for="switch_b"><span>Dim</span></label>
                    <label for="switch_c"><span>Dark</span></label>
                    <label for="switch_d"><span>Off</span></label>
                    <input id="switch_a" name="switch" ng-click="updateLight(1,'full')" checked type="radio">
                    <input id="switch_b" name="switch" ng-click="updateLight(1,'dim')" type="radio">
                    <input id="switch_c" name="switch" ng-click="updateLight(1,'dark')" type="radio">
                    <input id="switch_d" name="switch" ng-click="updateLight(1,'off')" type="radio">
                    <div class="light"><span></span></div>
                    <div class="dot"><span></span></div>
                    <div class="dene">
                        <div class="denem">
                            <div class="deneme">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
</body>
</html>
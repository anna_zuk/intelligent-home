'use strict';

angular.module('homeapp').factory('UserService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';

        var factory = {
            fetchAllUsers: fetchAllUsers,
            fetchUserWithId: fetchUserWithId,
            registerUser:registerUser
        };
        console.log('Factory created.');
        return factory;

        function fetchAllUsers() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listUsers').then(
                function ( result) {
                    deferred.resolve(result);
                }, function ( result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchUserWithId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'user/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function registerUser(userToRegister){
            var deferred = $q.defer();
            $http.post(RESTAPIURL + 'registerUserWithData', userToRegister).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(find, 'g'), replace);
        }
    }]);
<div id="cont2">
    <div id="menu-fixed2">
        <a href="#cont2">
            <i class="material-icons back">&#xe315;</i>
        </a>
        <a href="#menu-fixed2">
            <div class="logo2">
                <span></span>
            </div>
            <p class="pmenu2">MY HOMES</p>
        </a>
        <hr>
        <ul class="menu2">
            <a href="edit_home"><li><i class="material-icons">&#xe3c9;</i><p>Edit Home</p></li></a>
            <a href="add_home"><li><i class="material-icons">&#xe145;</i><p>Add Home</p></li></a>
            <a href="remove_home"><li><i class="material-icons">&#xe14c;</i><p>Remove Home</p></li></a>
        </ul>
    </div>
</div>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Terms</title>
</head>
<body>
    <%@include file="/WEB-INF/includes/Logo_Background.jsp"%>
<div class="box_terms">
    <%@include file="/WEB-INF/includes/TermsMenu.jsp"%>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>6. Assignment</h3>
    <br><br>
    <p class="Terms"> KOTI&reg; shall be permitted to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification or consent required. However, You shall not be permitted to assign, transfer, or subcontract any of Your rights and/or obligations under these Terms.</p>
    <br><br><br>
</div>
</body>
</html>
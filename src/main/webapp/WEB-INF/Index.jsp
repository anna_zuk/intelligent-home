<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>KOTI</title>
</head>
<body ng-app="homeapp">
    <%@include file="/WEB-INF/includes/Logo_Background.jsp"%>
<div ng-controller="UserController as ctrl">
    <div class="box_register">
        <h1>Registration</h1>
        <form ng-submit="ctrl.sendNewUser()" name="myForm">
            <input type="hidden" ng-model="ctrl.newUser.userId"/>
            <hr>
            <input type="text" ng-model="ctrl.newUser.userData.email" placeholder="Email" id="userEmail" required/>
            <input type="text" ng-model="ctrl.newUser.login" placeholder="Username" id="login" required/>
            <input type="password" ng-model="ctrl.newUser.passwordHash" placeholder="Password" id="userPassword" required/>
            <div class="gender">
                <input type="radio" id="male"/>
                <label for="male" class="radio">Male</label>
                <input type="radio" id="female" ng-model="ctrl.newUser.userData.isFemale"/>
                <label for="female" class="radio">Female</label>
            </div>
            <p class="Register">By clicking Register, You agree to our <a href="terms">Terms and Conditions</a>.</p>
            <button type="submit" class="register" value="Register" ng-disabled="myForm.$invalid">Register</button>
            <br><br>
            <hr>
            <h4>Already registered?</h4>
            <button type="button" onclick="window.location.href = 'login_form';" class="login">Login</button>
        </form>
    </div>
</div>
    <%@include file="/WEB-INF/includes/Footer.jsp"%>
</body>
</html>
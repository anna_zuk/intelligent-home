<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
    <title>Terms</title>
</head>
<body>
    <%@include file="/WEB-INF/includes/Logo_Background.jsp"%>
<div class="box_terms">
    <%@include file="/WEB-INF/includes/TermsMenu.jsp"%>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>4. No Warranties</h3>
    <br><br>
    <p class="Terms"> This Website is provided "as is", with all faults, and KOTI&reg; makes no express or implied representations or warranties, of any kind related to this Website or the materials contained on this Website. Additionally, nothing contained on this Website shall be construed as providing consult or advice to You.</p>
    <br><br><br>
</div>
</body>
</html>
package com.intelligent_home.service;

import com.intelligent_home.model.Home;

import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2018-01-10
 */

public interface HomeService {

    List<Home> getAllHomes();

    List<Long> getHomeRooms(long homeId);

    Home getHomeById(long homeId);

    boolean homeExists(String homeName);

    boolean addHome(long userId, Home home);

    void updateRoomTemperature(long roomId, int temperature);

    void updateRoomLight(long roomId, String light);

    void setAir(long roomId, boolean air);

    void setLocks(long roomId, boolean locks);

    void setInternet(long roomId, boolean internet);
}
